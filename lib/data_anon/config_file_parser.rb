require 'json'

module DataAnon
  class ConfigFileParser < Base
    attr_accessor :config

    NUMBERS_AND_LETTERS = [*'0'..'9', *'a'..'z', *'A'..'Z'].join.freeze

    def initialize(config_path)
      self.config = JSON.parse(File.read(config_path))
    rescue Errno::ENOENT, JSON::ParserError => e
      raise ArgumentError, e.message
    end

    def call
      DataAnon::ConfigValidator.call(config)
      parsed_config
    end

    private

    def parsed_config
      arr = []
      config.each do |table, methods|
        methods.each do |method_name, fields|
          fields.each do |field|
            arr << send(method_name, table, field)
          end
        end
      end
      arr
    end

    def delete_content(table, field)
      "update #{table} set #{field} = NULL;"
    end

    def replace_email(table, field)
      "update #{table} set #{field} = '#{field}+#{table}' || id || '@test.com';"
    end

    def scramble_data(table, field)
      "update #{table} set #{field} = translate(#{field}, '#{NUMBERS_AND_LETTERS}', '#{data_replacement_key}');"
    end

    def data_replacement_key
      @key ||= [].tap do |arr|
        arr << Array.new(10){ [*'0'..'9'].sample }
        arr << Array.new(26){ [*'a'..'z'].sample }
        arr << Array.new(26){ [*'A'..'Z'].sample }
      end.flatten.join
    end
  end
end
