require 'data_anon/version'
require 'data_anon/base'
require 'data_anon/anonymizer'
require 'data_anon/config_file_parser'
require 'data_anon/config_validator'
require 'data_anon/input_parser'

module DataAnon
end
