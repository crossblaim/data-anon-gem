RSpec::Matchers.define :output_message_and_exit do |message|
  match do |subject|
    system_exit = false
    expect do
      begin
        subject.call
      rescue SystemExit
        system_exit = true
      end
    end.to output("#{message}\n").to_stdout
    system_exit
  end

  supports_block_expectations
end

