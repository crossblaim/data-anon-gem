module DataAnon
  class ConfigValidator < Base
    attr_accessor :config_hash

    ALLOWED_CONFIG_METHODS = %w(delete_content replace_email scramble_data)

    def initialize(config_hash)
      self.config_hash = config_hash
    end

    def call
      config_hash.each do |table, methods|
        check_methods(methods.keys, table)
        methods.each do |method_name, fields|
          check_method_args(fields, method_name, table)
        end
      end
      true
    end

    private

    def check_methods(methods, table)
      no_methods_error(table) if methods.empty?
      unallowed_method_error(table) unless (methods - ALLOWED_CONFIG_METHODS).empty?
    end

    def check_method_args(fields, method_name, table)
      arguments_format_error(method_name, table) unless fields.is_a?(Array)
    end

    def no_methods_error(table)
      raise_error("No methods defined for #{table} table.")
    end

    def unallowed_method_error(table)
      raise_error("Unallowed method(s) detected for #{table} table.")
    end

    def arguments_format_error(method_name, table)
      raise_error("Fields for #{method_name} method (#{table} table) should be passed in array.")
    end
  end
end
