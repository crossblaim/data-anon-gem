# DataAnon
Postgres dump anonymization tool.

## Installation
    $ gem install data_anon

## Requirements
To use this gem, you must have `postgresql` installed.

## Usage
    $ data_anon config_file_path dump_file_path

### Available anonymization methods

**delete_content** - Sets chosen field to nil. It will raise an error if field in db have setted `null: false`.

**replace_email** - Generates new email address. For `primary_email` field from `users` table (user object id = 42) it will look like this: `primary_email+users42@test.com`.

**scramble_data** - Replacing data while keeping its structure intact.
Scrambled string `My favourite number is 42` looks like this `Xw tmxxrphcu grbwup hl 73`.

### Configuration file structure
``config_file_example.json``:

```json
{
  "table_name": {
    "delete_content": ["field_name"],
    "replace_email":  ["email_field_name", "another_email_field_name"],
    "scramble_data":  ["string_field_name", "text_string_name"]
  },
  "another_table_name": {
    "delete_content": ["field_name"],
    "replace_email":  ["email_field_name", "another_email_field_name"],
    "scramble_data":  ["string_field_name", "text_string_name"]
  },
  .
  .
  .
}
```
