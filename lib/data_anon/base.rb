module DataAnon
  class Base
    def self.call(*args)
      new(*args).call
    end

    private

    def raise_error(message)
      raise ArgumentError, message
    end
  end
end
