require 'pathname'

module DataAnon
  class Anonymizer < Base
    attr_accessor :config, :dump_path

    ALLOWED_DUMP_EXTENSIONS = %w(.sql).freeze
    TEMP_BASE = 'temporary_data_anon_base'.freeze
    ANONYMIZE_SQL_FILENAME = 'anonymize.sql'.freeze
    ANONYMIZED_DUMP_PATH = 'anonymized_dump.sql'.freeze

    def initialize(options)
      self.config = DataAnon::ConfigFileParser.call(options[:config_path])
      self.dump_path = options[:dump_path]
    end

    def call
      validate_dump_path
      setup_db
      anonymize_dump
      cleanup
      ANONYMIZED_DUMP_PATH
    end

    private

    def validate_dump_path
      path = Pathname.new(dump_path)
      dump_not_found_error unless path.file?
      file_type_error unless ALLOWED_DUMP_EXTENSIONS.include?(path.extname)
    end

    def dump_not_found_error
      raise_error("No such file - #{dump_path}")
    end

    def file_type_error
      raise_error("#{dump_path} have unallowed type. Allowed types: #{ALLOWED_DUMP_EXTENSIONS.join(' ')}")
    end

    def system_call(instruction)
      system(instruction) || exit
    end

    def setup_db
      system_call('echo Setting up database...')
      system("createdb #{TEMP_BASE}") || setup_db_instructions.each do |instruction|
        system_call(instruction)
      end
    end

    def anonymize_dump
      anonymize_sql_file
      anonymize_instructions.each do |instruction|
        system_call(instruction)
      end
    end

    def anonymize_sql_file
      @file ||= File.open(ANONYMIZE_SQL_FILENAME, 'w') do |file|
        config.each do |line|
          file.puts(line)
        end
        file
      end
    end

    def cleanup
      cleanup_instructions.each do |instruction|
        system_call(instruction)
      end
    end

    def setup_db_instructions
      [
        'echo Removing old database...',
        "dropdb #{TEMP_BASE}",
        'echo Creating new database...',
        "createdb #{TEMP_BASE}",
        'echo Database created'
      ]
    end

    def anonymize_instructions
      [
        'echo Anonymizing data...',
        "psql #{TEMP_BASE} < #{dump_path}",
        "psql #{TEMP_BASE} < #{ANONYMIZE_SQL_FILENAME}",
        "pg_dump -xO #{TEMP_BASE} > #{ANONYMIZED_DUMP_PATH}",
      ]
    end

    def cleanup_instructions
      [
        "dropdb #{TEMP_BASE}",
        "rm #{ANONYMIZE_SQL_FILENAME}"
      ]
    end
  end
end
