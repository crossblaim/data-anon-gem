require 'spec_helper'

describe DataAnon::InputParser do
  subject { described_class.call(arguments) }

  context 'no arguments' do
    let(:arguments) { [] }

    it 'should raise error' do
      expect{ subject }.to raise_error(SystemExit)
    end

    it 'should return proper message' do
      expect{ subject }.to output_message_and_exit(described_class::ARGS_ERROR)
    end
  end

  context 'invalid arguments' do
    let(:arguments) { ['config.json'] }

    it 'should raise error' do
      expect{ subject }.to raise_error(SystemExit)
    end

    it 'should return proper message' do
      expect{ subject }.to output_message_and_exit(described_class::ARGS_ERROR)
    end
  end

  context 'valid arguments' do
    let(:arguments) { ['config.json', 'dump.sql'] }

    it 'should return parsed input' do
      expect(subject).to eq({ config_path: arguments[0], dump_path: arguments[1] })
    end

    context 'checking version' do
      let(:arguments) { ['-v'] }

      it 'should exit the program' do
        expect{ subject }.to raise_error(SystemExit)
      end

      it 'should return version' do
        expect{ subject }.to output_message_and_exit(described_class::VERSION_INFO)
      end
    end
  end
end
