# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'data_anon/version'

Gem::Specification.new do |s|
  s.name          = "data_anon"
  s.version       = DataAnon::VERSION
  s.authors       = ["Adam Wieczorkowski"]
  s.email         = ["adam.wieczorkowski@naturaily.com"]
  s.summary       = "Summary of DataAnon."
  s.description   = "Description of DataAnon."
  s.homepage      = ""
  s.files         = `git ls-files`.split($\)
  s.executables   = ["data_anon"]
  s.test_files    = s.files.grep(%r{^(spec)/})
  s.require_paths = ["lib"]
  s.license       = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "json"
  s.add_development_dependency "rake"
  s.add_development_dependency "rspec"
  s.add_development_dependency "pry-byebug"
end
