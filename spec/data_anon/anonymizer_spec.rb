require 'spec_helper'

describe DataAnon::Anonymizer do
  let(:options) { { config_path: config_path, dump_path: dump_path } }
  let(:config_path) { 'spec/fixtures/config_2.json' }
  subject { described_class.call(options) }

  context 'valid options' do
    let(:dump_path) { 'spec/fixtures/dump.sql' }
    let(:replacement_key) do
      '6236726630mwneutxrhedqbgxmbplcrxbxwdLQCXVCKAFYMKXXHFYZVGSCNAJC'
    end

    it 'should not raise any error', :remove_anonymized_dump do
      expect{ subject }.to_not raise_error
    end

    it 'should create anonymized_dump file', :remove_anonymized_dump do
      anonymized_dump_path = Pathname.new(subject)

      expect(anonymized_dump_path.file?).to eq(true)
    end

    it 'should not contain original  data', :remove_anonymized_dump do
      original_dump_data = [
        'joe.doe@firefield.com', 'Joe Doe', '683156907', 'fgWVb56He3HMtuc',
        'mark.etno@firefield.com', 'Mark Etno', '921630562', 'Kts82BGc13bnwGt',
        'andrew.bent@firefield.com', 'Andrew Bent', '843679032', 'op6Hb6kE6xsd42B',
        'bob.olen@firefield.com', 'Bob Olen', '569489726', '8Nmlo27GNeM7soU'
      ]
      file = File.read(subject)

      original_dump_data.each do |data|
        expect(file).to_not include(data)
      end
    end

    it 'should contain anonymized data', :remove_anonymized_dump do
      allow_any_instance_of(DataAnon::ConfigFileParser).to receive(:data_replacement_key)
        .and_return(replacement_key)

      anonymized_dump_data = [
        'email+users1@test.com', 'Yxu Xxu', '636226066',
        'email+users2@test.com', 'Xmpd Vcgx', '03266626',
        'email+users3@test.com', 'Lgepub Qugc', '376660663',
        'email+users4@test.com', 'Qxw Hqug', '260730636'
      ]
      file = File.read(subject)

      anonymized_dump_data.each do |data|
        expect(file).to include(data)
      end
    end
  end

  context 'invalid dump_path' do
    let(:dump_path) { 'invalid/dump_path.sql' }

    it 'should raise error with proper message' do
      expect{ subject }.to raise_error(
        ArgumentError,
        "No such file - #{dump_path}"
      )
    end
  end

  context 'dump file with unallowed extension' do
    let(:dump_path) { 'spec/fixtures/invalid_extension_dump.txt' }

    it 'should raise error with proper message' do
      allowed_extensions = DataAnon::Anonymizer::ALLOWED_DUMP_EXTENSIONS.join(' ')
      expect{ subject }.to raise_error(
        ArgumentError,
        "#{dump_path} have unallowed type. Allowed types: #{allowed_extensions}"
      )
    end
  end
end
