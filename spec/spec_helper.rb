require 'rspec'
require 'data_anon'
require 'pry-byebug'

Dir[File.join(File.dirname(__dir__), 'spec/support/**/*.rb')].each { |f| require f }

RSpec.configure do |c|
  c.before { allow($stdout).to receive(:puts) }

  c.after(:each) do |example|
    if example.metadata[:remove_anonymized_dump]
      system("rm #{DataAnon::Anonymizer::ANONYMIZED_DUMP_PATH}")
    end
  end
end
