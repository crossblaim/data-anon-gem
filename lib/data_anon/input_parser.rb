module DataAnon
  class InputParser < Base
    attr_accessor :input

    ARGS_ERROR = 'DataAnon: Looks like you provided invalid arguments.'.freeze
    VERSION_INFO = "DataAnon #{DataAnon::VERSION}"

    def initialize(input)
      self.input = input
    end

    def call
      show_version_and_quit if check_version?
      validate_input
      parsed_input
    rescue ArgumentError
      display_error_and_quit
    end

    private

    def parsed_input
      {
        config_path: input[0],
        dump_path: input[1]
      }
    end

    def validate_input
      raise ArgumentError unless input.is_a?(Array)
      raise ArgumentError unless input.length > 1
    end

    def check_version?
      input.include?('-v')
    end

    def show_version_and_quit
      puts VERSION_INFO
      exit
    end

    def display_error_and_quit
      puts ARGS_ERROR
      exit
    end
  end
end
