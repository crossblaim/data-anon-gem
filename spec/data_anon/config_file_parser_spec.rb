require 'spec_helper'

describe DataAnon::ConfigFileParser do
  subject { described_class.call(config_path) }

  context 'valid .json file' do
    let(:config_path) { 'spec/fixtures/config.json' }
    let(:replacement_key) do
      '6236726630mwneutxrhedqbgxmbplcrxbxwdLQCXVCKAFYMKXXHFYZVGSCNAJC'
    end

    it 'should return proper data in array' do
      allow_any_instance_of(described_class).to receive(:data_replacement_key)
        .and_return(replacement_key)

      expect(subject).to be_kind_of(Array)
      [
        "update users set bio = translate(bio, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', '#{replacement_key}');",
        "update users set email = 'email+users' || id || '@test.com';",
        "update users set secondary_email = 'secondary_email+users' || id || '@test.com';",
        "update users set token = NULL;",
        "update admins set info = translate(info, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', '#{replacement_key}');",
        "update admins set invite_token = NULL;",
        "update admins set email = 'email+admins' || id || '@test.com';"
      ].each do |line|
        expect(subject).to include(line)
      end
    end
  end

  context 'invalid .json file' do
    let(:config_path) { 'spec/fixtures/invalid_config.json' }

    it 'should raise error' do
      expect{ subject }.to raise_error(ArgumentError) do |e|
        expect(e.cause.class).to eq(JSON::ParserError)
        expect(e.message).to include('unexpected token at')
      end
    end
  end

  context 'no file' do
    let(:config_path) { 'invalid_path' }

    it 'should raise error' do
      expect{ subject }.to raise_error(ArgumentError) do |e|
        expect(e.cause.class).to eq(Errno::ENOENT)
        expect(e.message).to include("No such file or directory @ rb_sysopen - #{config_path}")
      end
    end
  end
end
