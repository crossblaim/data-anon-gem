require 'spec_helper'

describe DataAnon::ConfigValidator do
  subject { described_class.call(config_hash) }

  context 'valid config_hash' do
    let(:config_hash) do
      {
        "users"=>{
          "scramble_data"=>["bio"],
          "replace_email"=>["email"],
          "delete_content"=>["token"]
        }
      }
    end

    it 'should return true' do
      expect(subject).to eq(true)
    end
  end

  context 'no methods for table' do
    let(:config_hash) { { "users"=>{} } }

    it 'should raise error' do
      expect{ subject }.to raise_error(
        ArgumentError,
        'No methods defined for users table.'
      )
    end
  end

  context 'config includes unallowed methods' do
    let(:config_hash) do
      {
        "users"=>{
          "scramble_data"=>["bio"],
          "invalid_method"=>["email"],
        }
      }
    end

    it 'should raise error' do
      expect{ subject }.to raise_error(
        ArgumentError,
        'Unallowed method(s) detected for users table.'
      )
    end
  end

  context 'wrong format of method arguments' do
    let(:config_hash) do
      {
        "users"=>{
          "scramble_data"=>["bio"],
          "replace_email"=>"email",
        }
      }
    end

    it 'should raise error' do
      expect{ subject }.to raise_error(
        ArgumentError,
        'Fields for replace_email method (users table) should be passed in array.'
      )
    end
  end
end
